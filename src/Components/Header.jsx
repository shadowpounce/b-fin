import styles from '@/styles/Header.module.scss'

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.logo}>B-Fin</div>
      <nav className={styles.nav}>
        <ul>
          <li className={styles.dropdown}>
            <a href="/">Переваги</a>
          </li>
          <li className={styles.dropdown}>
            <a href="/">Функціонал</a>
          </li>
          <li>
            <a href="/">Блог</a>
          </li>
          <li>
            <a href="/">Контакти</a>
          </li>
        </ul>
        <ul>
          <li>
            <a href="/">Вхід</a>
          </li>
          <li>
            <a href="/">Реєстрація</a>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header
