import styles from './Buttons.module.scss'

const ButtonPrimary = ({ type, children }) => {
  return (
    <button className={`${styles.button} ${styles[type]}`}>{children}</button>
  )
}

export default ButtonPrimary
