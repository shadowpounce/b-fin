import Head from 'next/head'
import Header from '@/Components/Header'
import styles from '@/styles/Home.module.scss'

import Welcome from './Home/Welcome/Welcome'
import Possibilities from './Home/Possibilities/Possibilities'
import Suitables from './Home/Suitables/Suitables'
import Security from './Home/Security/Security'

export default function Home() {
  return (
    <>
      <Head>
        <title>B-Fin - сервіс CRM + ERP</title>
        <meta
          name="description"
          content="Платформа B-FIN - это программа для ведения  мелкого и среднего бизнеса. Онлайн Сервис B-FIN является системой нового поколения ERP и позволяет постоянно контролировать финансы вашего бизнеса"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <Header />
        <Welcome />
        <Possibilities />
        <Suitables />
        <Security />
      </main>
    </>
  )
}
