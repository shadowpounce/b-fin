import styles from './Suitables.module.scss'

import b1 from './assets/b1.png'
import b2 from './assets/b2.png'
import icon1 from './assets/i1.svg'
import icon2 from './assets/i2.svg'
import icon3 from './assets/i3.svg'
import icon4 from './assets/i4.svg'
import icon5 from './assets/i5.svg'
import icon6 from './assets/i6.svg'

const Suitables = () => {
  return (
    <section className={styles.suitables}>
      <div className={styles.container}>
        <div className="section-head">
          <h2 className="flex-title">
            <span className="span">B-Fin</span>
            Для кого підійде?
          </h2>
          <span className="section-span">Вибирай найкраще!</span>
        </div>
        <div className={styles.layout}>
          <div className={styles.box}>
            <img src={b1.src} alt="" />
          </div>
          <div className={styles.box}>
            <h3>Для середнього бізнесу</h3>
            <p>
              Цей функціонал допомагає упорядкувати ваші продукти для ведення
              бізнесу та матеріали компанії. Нова ERP-система здатна{' '}
              <b>примножити прибуток</b> та покращити дотримання нормативних
              вимог за допомогою:
            </p>
            <ul>
              <li>Моніторингу </li>
              <li>Аналітики</li>
            </ul>
            <p>
              Надання всіх фінансових звітів у реальному часі Функціонал Б-ФІН,
              як і ERP значно спрощує та автоматизує процеси за допомогою
              роботизації та програм для обліку малого бізнесу, що допомагає в
              управлінні ресурсів та продуктів для бізнесу. Завдяки опціям B-FIN
              ви швидше отримаєте адаптивність, необхідну для початку
              оптимізації обліку малого та середнього бізнесу. ERP здатний{' '}
              <b>підвищити ефективність</b> роботи малого та середнього бізнесу
              за допомогою повного супроводу необхідними бухгалтерськими
              документами як: накладні, рахунки тощо. А також ви{' '}
              <b>моментально</b> отримаєте інформацію про стан рахунків,
              приходів та видатків коштів.
            </p>
          </div>
          <div className={styles.box}>
            <h3>Для малого та мiкро бiзнесу</h3>
            <p>
              <b>Легше</b> ведеться управлінський облік завдяки автоматизації
              фінансових результатів, доходів, витрат та платежів. Модулі
              ERP-системи застосовуються як у <b>малому</b>, так і в{' '}
              <b>середньому бізнесі</b> як програми для{' '}
              <span className="span">
                управління обліковою системою підприємства
              </span>
              . За допомогою повної адаптації системи під ваш мобільний, вам
              буде легше контролювати всю облікову систему
              підприємства.Онлайн-Сервіс-B-FIN надає управлінський облік для
              планування, аналізу та контролю відхилень у планових показниках.
              Гнучкі параметри доступу до інформаційної системи для будь-якого
              типу середнього бізнесу та малого підприємства. <b>Швидкість</b>{' '}
              роботи платформи допомагає швидше автоматизувати усі обліки малого
              підприємства. Автоматизація облікової системи підприємства
              переважно підвищить продуктивність, <b>покращить якість</b> та{' '}
              <b>збільшить прибуток</b>.
            </p>
          </div>
          <div className={styles.box}>
            <img src={b2.src} alt="" />
          </div>
        </div>
        <div className={styles.items}>
          <div className={styles.item}>
            <div className={styles.itemIcon}>
              <img src={icon1.src} alt="" />
            </div>
            <p className={styles.itemTitle}>Оптова торгівля</p>
          </div>
          <div className={styles.item}>
            <div className={styles.itemIcon}>
              <img src={icon2.src} alt="" />
            </div>
            <p className={styles.itemTitle}>Роздрібна торгівля</p>
          </div>
          <div className={styles.item}>
            <div className={styles.itemIcon}>
              <img src={icon3.src} alt="" />
            </div>
            <p className={styles.itemTitle}>Е-commerce</p>
          </div>
          <div className={styles.item}>
            <div className={styles.itemIcon}>
              <img src={icon4.src} alt="" />
            </div>
            <p className={styles.itemTitle}>Складський облік</p>
          </div>
          <div className={styles.item}>
            <div className={styles.itemIcon}>
              <img src={icon5.src} alt="" />
            </div>
            <p className={styles.itemTitle}>Виробництва</p>
          </div>
          <div className={styles.item}>
            <div className={styles.itemIcon}>
              <img src={icon6.src} alt="" />
            </div>
            <p className={styles.itemTitle}>Послуги</p>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Suitables
