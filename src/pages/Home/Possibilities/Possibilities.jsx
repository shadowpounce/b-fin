import styles from './Possibilities.module.scss'
import yt from '@/media/images/yt.png'
import p1 from './assets/1.svg'
import p2 from './assets/2.svg'
import p3 from './assets/3.svg'

import ButtonPrimary from '@/Components/Buttons/ButtonPrimary'

const Possibilities = () => {
  return (
    <section className={styles.possibilities}>
      <div className={styles.container}>
        <div className={styles.wrapper}>
          <div className={styles.possibility}>
            <div className={styles.body}>
              <img src={p1.src} alt="" />
              <h4>Фінансовий облік</h4>
              <ul>
                <li>Начисление зарплаты</li>
                <li>Взаиморасчеты с контрагентами</li>
                <li>Интеграция с онлайн банками</li>
                <li>Учет доходов и расходов</li>
                <li>Учет валютных операций</li>
              </ul>
            </div>
            <ButtonPrimary type={'two'}>Почати</ButtonPrimary>
          </div>
          <div className={styles.possibility}>
            <div className={styles.body}>
              <img src={p2.src} alt="" />
              <h4>Фінансовий облік</h4>
              <ul>
                <li>Начисление зарплаты</li>
                <li>Взаиморасчеты с контрагентами</li>
                <li>Интеграция с онлайн банками</li>
                <li>Учет доходов и расходов</li>
                <li>Учет валютных операций</li>
              </ul>
            </div>
            <ButtonPrimary type={'two'}>Почати</ButtonPrimary>
          </div>
          <div className={styles.possibility}>
            <div className={styles.body}>
              <img src={p3.src} alt="" />
              <h4>Фінансовий облік</h4>
              <ul>
                <li>Начисление зарплаты</li>
                <li>Взаиморасчеты с контрагентами</li>
                <li>Интеграция с онлайн банками</li>
                <li>Учет доходов и расходов</li>
                <li>Учет валютных операций</li>
              </ul>
            </div>
            <ButtonPrimary type={'two'}>Почати</ButtonPrimary>
          </div>
        </div>
        <h4>
          Та багато корисного дiзнавайтеся з нашого{' '}
          <span className="span">навчального блогу</span>
        </h4>
        <div className={styles.row}>
          <p>Або вивчайте на канале</p>
          <img src={yt.src} alt="" />
        </div>
      </div>
    </section>
  )
}

export default Possibilities
