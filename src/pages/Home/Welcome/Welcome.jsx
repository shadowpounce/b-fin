import styles from './Welcome.module.scss'
import yt from '@/media/images/yt.png'
import devices from './assets/devices.png'

import ButtonPrimary from '@/Components/Buttons/ButtonPrimary'

const Welcome = () => {
  return (
    <section className={styles.welcome}>
      <div className={styles.container}>
        <div className={styles.content}>
          <h1>B-Fin - ВЕЛИКI МОЖЛИВОСТI ДЛЯ ВСIХ!</h1>
          <p>
            <span className="span">B-Fin</span> - це не просто онлайн сервіс
            управлінського обліку.  
            <b>
              Це екосистема для бізнесу, яка об'єднує користувачів і спрощує
              ведення об ліку
            </b>
          </p>
          <div className={styles.row}>
            <ButtonPrimary type={'one'}>Почати</ButtonPrimary>
            <p>Дивитися на</p>
            <img src={yt.src} alt="" />
          </div>
        </div>
        <img src={devices.src} alt="" className={styles.welcomeImage} />
      </div>
    </section>
  )
}

export default Welcome
