import styles from './Security.module.scss'

import securityImg from './assets/img.png'
import icon1 from './assets/i1.svg'
import icon2 from './assets/i2.svg'
import icon3 from './assets/i3.svg'
import icon4 from './assets/i4.svg'
import icon5 from './assets/i5.svg'

const Security = () => {
  return (
    <section className={styles.security}>
      <div className={styles.container}>
        <div className="section-head">
          <h2 className="flex-title">Ваші дані у безпеці тому що:</h2>
          <span className="section-span">Нам довіряють!</span>
        </div>
        <div className={styles.wrapper}>
          <div className={styles.imageBlock}>
            <img src={securityImg.src} alt="" />
            <h4>Безпека</h4>
          </div>
          <ul className={styles.reasons}>
            <li>
              <img src={icon1.src} alt="" />
              <p>
                Schrift дотримуючись кращих практик зберігання паролів
                користувача, ніколи не зберігаючи паролі в оригінальному
                вигляді, а тільки в перетвореному, безпечному вигляді із
                застосуванням одностороннього хешування з "сіллю".
              </p>
            </li>
            <li>
              <img src={icon2.src} alt="" />
              <p>
                Пристрій, який зазвичай використовується для входу,
                ідентифікується системою. У разі доступу з нового пристрою, на
                email користувача надсилається повідомлення з детальною
                інформацією про підключення. Унеможливлюється підміна
                авторизованого пристрою із застосуванням викрадених даних цього
                пристрою
              </p>
            </li>
            <li>
              <img src={icon3.src} alt="" />
              <p>
                При введенні більше 5 разів неправильного пароля аккаунт
                користувача блокується на 5 хвилин і на його email надходить
                повідомлення про це. Причому система не розкриває інформації про
                те, чи існує за введеним email зареєстрований аккаунт.
              </p>
            </li>
            <li>
              <img src={icon4.src} alt="" />
              <p>
                Підключення клієнтів шифруються за протоколом HTTPS/TLS (TLS 1.2
                або вище), тобто весь трафік між клієнтом та нами захищений під
                час передачі
              </p>
            </li>
            <li>
              <img src={icon5.src} alt="" />
              <p>
                Дані наших клієнтів розміщені на надійних серверах, розташованих
                у Німеччині, та зашифровані з використанням ключів AES-256
              </p>
            </li>
          </ul>
        </div>
      </div>
    </section>
  )
}

export default Security
